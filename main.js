// Constructor Declaration (Section 1)
let ToDo = function (id) {
	
	let heading, add, todoList, items, template;
	
	items = document.getElementsByClassName('todo-list__item');	

	heading = elCr(
		'div', {class: 'heading'},
			elCr('span', {}, 'ToDo')
		);

	add = elCr(
		'div', {class: 'add'},
			elCr('input', {type: 'text', class: 'add-input'}),
			elCr('button', {class:"btn btn-add"}, 'Add')
		);

	todoList = elCr(
		'ul', {class: 'todo-list'}, 
			elCr('div', { class: 'warning-block' }, '4 items are max'));
	
	
	template = elCr(
		'div', {class: 'todo'}, heading, add, todoList,
			elCr('button', {class: 'todo__del-btn'}, 'Delete all')
		);
	


		
	function creator() {
		if (items.length <= 3) {
			let value = document.querySelector('.add-input').value;
			let item = elCr(
			'li', { class: 'todo-list__item' },
				elCr('span', { class: 'todo-list__text' }, value),
				elCr('div', { class: 'todo-list__actions' },
					elCr('button', { class: 'btn todo-list__btn-hold' }, 'Hold'),
					elCr('button', { class: 'btn todo-list__btn-del' }, '✖')
				)
			);
			todoList.appendChild(item);
			
			hold();
			deleteTodo();		

		} else {

			document.querySelector('.btn-add').disabled = true;
			document.querySelector('.warning-block').style.display='flex';

		}
	}
	
	function hold() {
		let hold = document.querySelectorAll('.todo-list__btn-hold');
		for (let i = 0; i < hold.length; i++) {
				hold[i].addEventListener('click', function() {
					let h = hold[i].parentElement;
					h.parentElement.classList.add('holding');
			});
		}
	}
	
	function deleteTodo() {
		let del = document.querySelectorAll('.todo-list__btn-del');
			for (let i = 0; i < del.length; i++) {
				del[i].addEventListener('click', function() {
				del[i].closest(".todo-list__item").remove();
				document.querySelector('.btn-add').disabled = false;
				document.querySelector('.warning-block').style.display='none';
			});
		}
	}

	function deleteAll() {
		let del = document.getElementsByClassName('todo-list__item');
		for (let i = 0; i < del.length;) {
			if(del[i].classList.value == 'todo-list__item holding'){
				i++;
				continue;
			} else {
				todoList.removeChild(del[i])
				document.querySelector('.btn-add').disabled = false;
				document.querySelector('.warning-block').style.display='none';
			};
		}
	}

	/*Create elements*/ 
	function elCr(type, attributes, ...children) {

		const el = document.createElement(type)
		for (key in attributes) {
			el.setAttribute(key, attributes[key])
		}

		children.forEach(child => {
			if (typeof child === 'string') {
				el.appendChild(document.createTextNode(child))
			} else {
				el.appendChild(child)
			}
		})
		return el
	}

	function init(container) {
		
		container.appendChild(template);
		
		let btnAdd = document.querySelector('.btn-add');
		btnAdd.addEventListener('click', creator);

		let btnDel = document.querySelector('.todo__del-btn');
		btnDel.addEventListener('click', deleteAll);

	}

	return {

		init: init

	}

};

